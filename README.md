# http_server_container

> comienza a 'dockerizar' este repo siempre y cuando tengas 'dockerizado' el proyecto telnet_container

Partiendo de una imagen node:latest, vamos a generar una imagen docker `http_server_container` que sea capaz de levantar un servidor web haciendo uso del paquete [http-server de npm](https://www.npmjs.com/package/http-server). Una vez generada la imagen, comprueba que las salidas corresponden:

```bash
➜  http_server_container git:(main) ✗ docker run -it --rm http_server_container
usage: http-server [path] [options]

options:
  -p --port    Port to use. If 0, look for open port. [8080]
  -a           Address to use [0.0.0.0]
  -d           Show directory listings [true]
  -i           Display autoIndex [true]
  -g --gzip    Serve gzip files when possible [false]
  -b --brotli  Serve brotli files when possible [false]
               If both brotli and gzip are enabled, brotli takes precedence
  -e --ext     Default file extension if none supplied [none]
  -s --silent  Suppress log messages from output
  --cors[=headers]   Enable CORS via the "Access-Control-Allow-Origin" header
                     Optionally provide CORS headers list separated by commas
  -o [path]    Open browser window after starting the server.
               Optionally provide a URL path to open the browser window to.
  -c           Cache time (max-age) in seconds [3600], e.g. -c10 for 10 seconds.
               To disable caching, use -c-1.
  -t           Connections timeout in seconds [120], e.g. -t60 for 1 minute.
               To disable timeout, use -t0
  -U --utc     Use UTC time format in log messages.
  --log-ip     Enable logging of the client's IP address

  -P --proxy       Fallback proxy if the request cannot be resolved. e.g.: http://someurl.com
  --proxy-options  Pass options to proxy using nested dotted objects. e.g.: --proxy-options.secure false

  --username   Username for basic authentication [none]
               Can also be specified with the env variable NODE_HTTP_SERVER_USERNAME
  --password   Password for basic authentication [none]
               Can also be specified with the env variable NODE_HTTP_SERVER_PASSWORD

  -S --tls --ssl   Enable secure request serving with TLS/SSL (HTTPS)
  -C --cert    Path to TLS cert file (default: cert.pem)
  -K --key     Path to TLS key file (default: key.pem)

  -r --robots        Respond to /robots.txt [User-agent: *\nDisallow: /]
  --no-dotfiles      Do not show dotfiles
  --mimetypes        Path to a .types file for custom mimetype definition
  -h --help          Print this list and exit.
  -v --version       Print the version and exit.
```



```bash
cd web
➜  web git:(main) ✗ ls -lart
total 8
drwxr-xr-x  6 jose  staff  192 13 mar 08:38 ..
drwxr-xr-x  3 jose  staff   96 13 mar 08:38 .
-rw-r--r--  1 jose  staff   12 13 mar 08:38 index.html

➜  web git:(main) ✗ docker run -it -v ${pwd}:/app -p 8080:8080 --rm http_server_container /app 8080
Starting up http-server, serving /app

http-server version: 14.1.1

http-server settings:
CORS: disabled
Cache: 3600 seconds
Connection Timeout: 120 seconds
Directory Listings: visible
AutoIndex: visible
Serve GZIP Files: false
Serve Brotli Files: false
Default File Extension: none

Available on:
  http://127.0.0.1:8080
  http://172.17.0.2:8080
Hit CTRL-C to stop the server
```

- ¿Serías capaz de añadir `docker run -it -v ${pwd}:/app -p 8080:8080 --rm http_server_container /app 8080` como un alias de PowerShell con el nombre http-server-docker?